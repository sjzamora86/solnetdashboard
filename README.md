# solnet-dashboard

Please find the details below:

This is my version of Solnet Dashboard. This application is made in AngularJS, NodeJS, and SailsJS.

Installation:
1. On the project path run npm install
2. To start the application run node app.js
3. To access open web browser: http://localhost:1337

File Structure
--------------------------------
api - This composed of server files made for REST API
assets - This composed of raw asset files mainly images, JS dependencies, CSS, templates etc
config - SailsJS config files
data - This is where the mocked data (JSON) are saved
node_modules - NodeJS Libraries installed
source - This composed of source files of the AngularJS
tasks - This composed of configuration files mainly for Grunt to use for compilation
views - This composed of pages that will be using for the application. In this case, it will only have 1 file, because it is a single-page application

Additional Information
--------------------------------
In pipeline.js
This is where the dependencies are declared to insert by the linker in the index.jade

In config/bootstrap.js
Initialize Dashboard Service to load the dummy data to cache

In config/blueprint.js
This is where you can configure the API that you want to provide. You can add prefix in this file