(function(){

 'use strict';
 
  angular
       .module('solnet.components')
       .directive('statswidget', StatsWidgetDirective);
       
  function StatsWidgetDirective()
    {
        return {
                restrict : 'E',
                replace: true,
                scope: {
                    hours: "=",
                    conversation: "=",
                    people: "="
                },
                templateUrl: 'components/widget/statswidget.html',
                link: function(scope) {
                    scope.$watch('hours', function(){
                        scope.total_hours = 0;
                        scope.total_conversation = 0;
                        scope.ghours_labels = [];
                        scope.ghours_series = [];
                        scope.ghours_data = [];

                        scope.gconv_labels = [];
                        scope.gconv_series = [];

                        angular.forEach(scope.hours, function(v, k){
                            
                            scope.ghours_series.push(v.series);
                            scope.ghours_data.push(v.value);
                            
                            angular.forEach(v.value, function(v1, k1){
                                scope.total_hours += v1;

                                if(scope.ghours_labels.length < v.value.length)
                                {
                                    scope.ghours_labels.push(k1.toString());
                                }
                            });
                        });

                        angular.forEach(scope.conversation, function(v, k){

                            angular.forEach(v, function(v1, k1){
                                scope.total_conversation += v1;

                                if(scope.gconv_labels.length < v.length)
                                {
                                    scope.gconv_labels.push(k1.toString());
                                }
                               
                            });

                            scope.gconv_series.push(k.toString());
                        });
                    });
                }
                
            };
    }

})();
