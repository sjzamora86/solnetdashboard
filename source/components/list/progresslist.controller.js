(function(){

 'use strict';
 
  angular
       .module('solnet.components')
       .directive('progresslist', ProgressListDirective);
       
  function ProgressListDirective()
    {
        return {
                restrict : 'E',
                replace: true,
                scope: {
                    data: "="
                },
                templateUrl: 'components/list/progresslist.html'
            };
    }

})();
