(function(){

  'use strict';
  
  angular
       .module('solnet.components')
       .directive('linegraph', LineGraphDirective);
       
  function LineGraphDirective()
    {
        return {
                restrict : 'E',
                replace: true,
				scope: {
                    labels: "=",
					series: "=",
                    data: "=",
                    xaxis: "=",
                    yaxis: "=",
                    hline: "=",
                    vline: "=",
                    fill: "="
                },
                templateUrl: 'components/graph/linegraph.html',
                link: function(scope) {
                    var xaxis_opt = { display: true };
                    var yaxis_opt = { display: true };

                    var hline_opt = { gridLines: { drawOnChartArea: true }};
                    var vline_opt = { gridLines: { drawOnChartArea: true }};

                    var scale_opt = { xAxes: [], yAxes: []};
                    
                    var fill_opt = true;
                    var datasetOverride = [];

                    if(scope.xaxis != undefined)
                    {
                        xaxis_opt.display = scope.xaxis;
                        scale_opt.xAxes.push(xaxis_opt);
                    }

                    if(scope.yaxis != undefined)
                    {
                        yaxis_opt.display = scope.yaxis;
                        scale_opt.yAxes.push(yaxis_opt);
                    }

                    if(scope.hline != undefined)
                    {
                        hline_opt.gridLines.drawOnChartArea = scope.hline;
                        scale_opt.yAxes.push(hline_opt);
                    }

                    if(scope.vline != undefined)
                    {
                        vline_opt.gridLines.drawOnChartArea = scope.vline;
                        scale_opt.xAxes.push(vline_opt);
                    }

                    if(scope.fill != undefined)
                    {
                        fill_opt = scope.fill;
                    }

                    scope.onClick = function (points, evt) {
                        console.log(points, evt);
                    };
                    scope.chartOption = {
                        scales: scale_opt
                    };

                    angular.forEach(scope.data, function(v, k){
                        datasetOverride.push({ fill: fill_opt })
                    });

                    scope.datasetOverride = datasetOverride;

                }
            };
    }
})();
