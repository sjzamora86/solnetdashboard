(function(){

 'use strict';
 
  angular
       .module('solnet.components')
       .directive('doughnutgraph', DoughnutGraphDirective);
       
  function DoughnutGraphDirective()
    {
        return {
                restrict : 'E',
                replace: true,
                scope: {
                    labels: "=",
                    colors: "=",
                    data: "="                    
                },
                templateUrl: 'components/graph/doughnutgraph.html',
                link: function(scope) {
                    scope.onClick = function (points, evt) {
                        console.log(points, evt);
                    };
                    scope.chartOption = { animation: false };
                }
                
            };
    }

})();
