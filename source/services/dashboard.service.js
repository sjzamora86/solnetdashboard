(function () {
    'use strict';

    angular
        .module('solnet.services')
        .factory('DashboardService', DashboardService);

    DashboardService.$inject = ['$http'];
    function DashboardService($http) {
        var service = {};
        var options = {
            method: '',
            url: ''
        };

        service.getWorkingHours = getWorkingHours;
        service.getProgress = getProgress;
        service.getClientHours = getClientHours;
        service.getClientInvoice = getClientInvoice;
        service.getOverdue = getOverdue;
        service.getProjectSummary = getProjectSummary;
        service.getStatistics = getStatistics;

        return service;

        function getWorkingHours(start, end) {
            
            options = {
                method: 'POST',
                url: '/solnet/api/dashboard/search',
                data: {start: start, end: end, tag: "working_hours"}                
            }
            
            return $http(options).then(handleSuccess, handleError('Error getting working hours'));
        }

        function getProgress(start, end) {
            
            options = {
                method: 'POST',
                url: '/solnet/api/dashboard/search',
                data: {start: start, end: end, tag: "progress"}                
            }
            
            return $http(options).then(handleSuccess, handleError('Error getting progress'));
        }

        function getClientInvoice() {
            
            options = {
                method: 'GET',
                url: '/solnet/api/dashboard/clientinvoice'                
            }
            
            return $http(options).then(handleSuccess, handleError('Error getting client invoice'));
        }

        function getClientHours( duration ) {
            
            options = {
                method: 'POST',
                url: '/solnet/api/dashboard/clienthours' ,
                data: { duration: duration }                
            }
            
            return $http(options).then(handleSuccess, handleError('Error getting client hours'));
        }

        function getOverdue( duration ) {
            
            options = {
                method: 'POST',
                url: '/solnet/api/dashboard/overdue' ,
                data: { duration: duration }                
            }
            
            return $http(options).then(handleSuccess, handleError('Error getting overdue'));
        }

        function getProjectSummary() {
            options = {
                method: 'GET',
                url: '/solnet/api/dashboard/projectsummary' 
            }
            
            return $http(options).then(handleSuccess, handleError('Error getting project summary'));
        }

        function getStatistics(start, end) {
            options = {
                method: 'POST',
                url: '/solnet/api/dashboard/search',
                data: {start: start, end: end, tag: "statistics"}                
            }
            
            return $http(options).then(handleSuccess, handleError('Error getting project statistics'));
        }

        // private functions

        function handleSuccess(res) {
            return res.data;
        }

        function handleError(error) {
            return function () {
                return { success: false, message: error };
            };
        }
    }

})();
