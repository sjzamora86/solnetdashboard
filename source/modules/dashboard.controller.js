(function(){

  'use strict';

   angular
        .module('solnet.modules')
        .controller('DashboardController', DashboardController );
  
  DashboardController.$inject = ['DashboardService'];
  function DashboardController(DashboardService) {
      
    var vm = this;

    vm.wh_week_selected = undefined;
    vm.pr_week_selected = undefined;
    vm.timeline1_selected = undefined;
    vm.timeline2_selected = undefined;
    vm.project_summary = [];
    vm.statistics =
    {
      hours: [],
      conversation: [],
      people: []
    };
    
    vm.wh_week_changed = function()
    {
      var range = compute_range(parseInt(vm.wh_week_selected) + 7);
      getWorkingHours(range.start, range.end);
    }

    vm.timeline1_changed = function()
    {
      if(vm.timeline1_selected == undefined || vm.pr_week_selected == undefined) return;

      var range = compute_range(parseInt(vm.timeline1_selected), parseInt(vm.pr_week_selected));
      getProgress(range.start, range.end);
      getStatistics(range.start, range.end);
    }

    vm.timeline2_changed = function()
    {
      getClientHours(parseInt(vm.timeline2_selected));
      getOverdue(parseInt(vm.timeline2_selected));
    }

    var compute_range = function(duration, expectedEndTime)
    {
      var endTime = 0;
      var startTime = 0;

      var today = new Date();
      today.setHours(0);
      today.setMinutes(0);
      today.setSeconds(0);
      today.setMilliseconds(0);

      if(expectedEndTime == undefined)
      {       
          endTime = today.getTime() / 1000;          
      }
      else
      {
          endTime = (today.getTime() / 1000) - (expectedEndTime * 86400);
      }

      startTime = endTime - (duration * 86400);      

      return { start: startTime, end: endTime };
    }

    var getWorkingHours = function(start, end)
    {
      DashboardService.getWorkingHours(start, end)
      .then(function(response){
          vm.wavylines = response;
      });
    }

    var getProgress = function(start, end)
    {
      DashboardService.getProgress(start, end)
      .then(function(response){
          vm.progress = response;
      });
    }

    var getClientInvoice = function()
    {
      DashboardService.getClientInvoice()
      .then(function(response){
          vm.client_invoice = response;
      });
    }

    var getClientHours = function(duration)
    {
      DashboardService.getClientHours(duration)
      .then(function(response){
          vm.client_hours = response;
      });
    }

    var getOverdue = function(duration)
    {
      DashboardService.getOverdue(duration)
      .then(function(response){
          vm.overdue = response;
      });
    }

    var getProjectSummary = function()
    {
      DashboardService.getProjectSummary()
      .then(function(response){
          vm.project_summary = response;
      });
    }

    var getStatistics = function(start, end)
    {
      DashboardService.getStatistics(start, end)
      .then(function(response){
          vm.statistics = response;
      });
    }
    
    //call this function to initialize the dashboard and get initial data from the server
    var initialize = function()
    {
        //initial date should be current week with timeline of 7 days

        var initial_date_range = compute_range(7);
        
        vm.timeline_options = [
        {
          value: 6,
          label: "7 days"
        },
        {
          value: 4,
          label: "5 days"
        },
        {
          value: 2,
          label: "3 days"
        }
      ];

      vm.week_options = [
        {
          value: 0,
          label: "THIS WEEK"
        },
        {
          value: 7,
          label: "LAST WEEK"
        },
        {
          value: 14,
          label: "2 WEEKS AGO"
        },
        {
          value: 21,
          label: "3 WEEKS AGO"
        },
        {
          value: 30,
          label: "LAST MONTH"
        }
      ];

      //DEFAULT
      getStatistics(initial_date_range.start, initial_date_range.end);
      getWorkingHours(initial_date_range.start, initial_date_range.end);
      getProgress(initial_date_range.start, initial_date_range.end);
      getClientHours(7);
      getClientInvoice();
      getOverdue(7);
      getProjectSummary();
    }    

    initialize();
      
  }
  
  

})();