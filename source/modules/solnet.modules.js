(function () {
    'use strict';

    angular
        .module('solnet.modules', ['ui.router'])
        .config(function($stateProvider, $urlRouterProvider){
            $stateProvider
                .state('home', {
                    url: "/home",
                    views: {
                        "contentview": {
                            templateUrl: "templates/dashboard.html",
                            controller: 'DashboardController',
                            controllerAs: 'vm'
                        }
                    }
                })
                .state('timemngt', {
                    url: "/timemngt",
                    views: {
                        "contentview": {
                            templateUrl: "templates/timemngt.html",
                            controller: 'TimeMngtController',
                            controllerAs: 'vm'
                        }
                    }                    
                })
                .state('inbox', {
                    url: "/inbox",
                    views: {
                        "contentview": {
                            templateUrl: "templates/inbox.html",
                            controller: 'InboxController',
                            controllerAs: 'vm'
                        }
                    }
                })
                .state('calendar', {
                    url: "/calendar",
                    views: {
                        "contentview": {
                            templateUrl: "templates/calendar.html",
                            controller: 'CalendarController',
                            controllerAs: 'vm'
                        }
                    }
                })
                .state('analytics', {
                    url: "/analytics",
                    views: {
                        "contentview": {
                            templateUrl: "templates/analytics.html",
                            controller: 'AnalyticsController',
                            controllerAs: 'vm'
                        }
                    }
                })
                .state('settings', {
                    url: "/settings",
                    views: {
                        "contentview": {
                            templateUrl: "templates/settings.html",
                            controller: 'SettingsController',
                            controllerAs: 'vm'
                        }
                    }
                });
        });
})();