(function () {
    'use strict';
 
    angular
        .module('solnet')
        .controller('AppController', AppController);
 
    AppController.$inject = ['$rootScope', '$scope'];
    function AppController($rootScope, $scope) {
        var vm = this;
        vm.$rootScope = $rootScope;
        vm.$scope = $scope;

        vm.menus = [
            {
                href: "#/home",
                icon: "fa-dashboard",
                title: "Home"
            },
            {
                href: "#/timemngt",
                icon: "fa-clock-o",
                title: "Time Management"
            },
            {
                href: "#/inbox",
                icon: "fa-inbox",
                title: "Inbox"
            },
            {
                href: "#/calendar",
                icon: "fa-calendar",
                title: "Calendar"
            },
            {
                href: "#/analytics",
                icon: "fa-line-chart",
                title: "Analytics"
            },
            {
                href: "#/settings",
                icon: "fa-cog",
                title: "Settings"
            }
        ];

        vm.selected = undefined;
        vm.tags = ["Auckland","Wellington","Christchurch","Hamilton","Tauranga","Napier-Hastings","Dunedin","Palmerston North","Nelson","Rotorua","New Plymouth","Whangarei","Invercargill","Whanganui","Gisborne"];
    }
 
})();