(function () {
    'use strict';

    angular
        .module('solnet')
        .config(routerProviderConfig);
        
   
    routerProviderConfig.$inject = ['$stateProvider', '$urlRouterProvider'];
    function routerProviderConfig($stateProvider, $urlRouterProvider)
    {
        $urlRouterProvider.otherwise("/home");
    }
    
})();