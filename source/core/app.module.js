(function () {
    'use strict';

    angular
        .module('solnet', ['ui.router', 'ui.bootstrap', 'chart.js', 'solnet.modules', 'solnet.components', 'solnet.services' ]);
})();