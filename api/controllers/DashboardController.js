/**
 * DashboardController
 *
 * @description :: Entry Point of api call
 */

module.exports = {
	search: function(req, res)
    {
        var criteria = {
            start: req.body.start,
            end: req.body.end,
            tag: req.body.tag
        };

        var result = undefined;

        if(criteria.tag == "working_hours")
        {
            result = DashboardService.getWorkingHours(criteria.start, criteria.end);            
        }
        else if(criteria.tag == "progress")
        {
            result = DashboardService.getDailyProgress(criteria.start, criteria.end);
        }
        else if(criteria.tag == "statistics")
        {
            result = DashboardService.getStatistics(criteria.start, criteria.end);
        }
        
        if(result != undefined)
        {
            res.ok(result);
        }
        else
        {
            res.serverError({message: "result undefined"});
        }
    },
    clientinvoice: function(req, res)
    {
        var result = DashboardService.getClientInvoice();
        res.ok(result);
    },
    clienthours: function(req, res)
    {
        var result = DashboardService.getClientHours(req.body.duration);
        res.ok(result);
    },
    overdue: function(req, res)
    {
        var result = DashboardService.getOverdue(req.body.duration);
        res.ok(result);
    },
    projectsummary: function(req, res)
    {
        var result = DashboardService.getProjectSummary();
        res.ok(result);
    }
};

