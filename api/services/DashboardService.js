/**
 * Dashboard Service
 *
 * @description :: This will handle the searching of the criterias in dummy JSON files. All server side logic will be handled by this service
 */
var jsonfile = require('jsonfile');

var cache_project;
var cache_people;
var cache_overdue;
var cache_client_hours;
var cache_client_invoice;

module.exports = {
	init: function()
    {
        jsonfile.readFile("data/project.json", function(err, data) {
            
            if(err == undefined)
            {
                cache_project = data;
            }
        });

        jsonfile.readFile("data/people.json", function(err, data) {
            
            if(err == undefined)
            {
                cache_people = data;
            }
        });

        jsonfile.readFile("data/overdue.json", function(err, data) {
            
            if(err == undefined)
            {
                cache_overdue = data;
            }
        });

        jsonfile.readFile("data/client_hours.json", function(err, data) {
            
            if(err == undefined)
            {
                cache_client_hours = data;
            }
        });

        jsonfile.readFile("data/client_invoice.json", function(err, data) {
            
            if(err == undefined)
            {
                cache_client_invoice = data;
            }
        });
    },
    getWorkingHours: function(start, end)
    {
        var returnObject = {
            label: [],
            series: [],
            data: []
        }

        cache_project.forEach(function(element, index) {
            
            returnObject.series.push(element.name);
            
            var working_hours = element["working_hours"];
            var wh_data = [];
            var counter = 0;
            
            while(counter < working_hours.length)
            {
                var time = parseInt(working_hours[counter].datetime);
                if(time >= start && time <= end)
                {
                    if(index == 0)
                    {
                        var dateTime = new Date(time * 1000);
                        returnObject.label.push(dateTime.getDate() + "." + (dateTime.getMonth() + 1))
                    }

                    wh_data.push(parseFloat(working_hours[counter].value));
                }
                counter++;
            }

            returnObject.data.push(wh_data);

        });

        return returnObject;
    },
    getDailyProgress: function(start, end)
    {
        var returnObject = {
            label: [],
            series: [],
            data: []
        }

        cache_project.forEach(function(element, index) {
            
            returnObject.series.push(element.name);
            
            var progress = element["progress"];
            var pr_data = [];
            var counter = 0;
            
            while(counter < progress.length)
            {
                var time = parseInt(progress[counter].datetime);
                if(time >= start && time <= end)
                {
                    if(index == 0)
                    {
                        var dateTime = new Date(time * 1000);
                        returnObject.label.push(dateTime.getDate() + "." + (dateTime.getMonth() + 1))
                    }

                    pr_data.push(parseFloat(progress[counter].value));
                }
                counter++;
            }

            returnObject.data.push(pr_data);

        });

        return returnObject;
    },
    getClientInvoice: function()
    {
        return cache_client_invoice;
    },
    getClientHours: function(duration)
    {
        var returnObject = {
            "labels": cache_client_hours.labels,
            "data": [],
            "colors": cache_client_hours.colors,
            "total": 0
        };

        for(var a=0; a < cache_client_hours.labels.length; a++)
        {
            var hr_total = 0;
            for(var b=0; b < duration; b++)
            {
                var data_seq = cache_client_hours.data[b];
                hr_total += data_seq[a];
            }
            
            returnObject.data.push((hr_total).toFixed(2));
            returnObject.total += hr_total;
        }

        returnObject.total = returnObject.total.toFixed(2)

        return returnObject;
    },
    getOverdue: function(duration)
    {
        var returnObject = {
            label: [],
            series: ["-"],
            data: [],
            total: 0
        }

        var overdue_total = 0;
        var tmp_data = [];

        for(var a=0; a < duration; a++)
        {
            overdue_total += cache_overdue.data[a];
            tmp_data.push(cache_overdue.data[a]);

            returnObject.label.push(a);
                        
        }

        returnObject.data.push(tmp_data);
        returnObject.total = overdue_total.toFixed(2);

        return returnObject;
    },
    getProjectSummary: function()
    {
        var returnObject = [];

        cache_project.forEach(function(element, index) {
            
            var prj_updated_time = new Date(element.updated * 1000);

            var prj_itm = {
                image: element.image,
                title: element.name,
                description: element.description,
                last_updated: prj_updated_time.toLocaleString(),
                duration: 0,
                conversation: 0,
                progress: 0
            };

            var working_hours = element["working_hours"];
            var progress = element["progress"];
            var conversation = element["conversation"];
            var summary = [ working_hours, progress, conversation ];
            var prop = ["duration", "progress", "conversation"];

            for(var a=0; a < prop.length; a++)
            {
                var collection = summary[a];
                for(var b=0; b < collection.length; b++)
                {
                    prj_itm[prop[a]] += parseFloat(collection[b].value);
                }   
            }

            returnObject.push(prj_itm);

        });

        return returnObject;
    },
    getStatistics: function(start, end)
    {
        var returnObject = {
            hours: [],
            conversation: [],
            people: []
        }       

        cache_project.forEach(function(element, index) {
            
            var conversation_itm = [];
            var hr_itm = {
                series: "",
                value: []
            }
            hr_itm.series = element.name;
            
            var working_hours = element["working_hours"];
            var conversation = element["conversation"];
            var people = element["people"];
            var counter = 0;
            
            while(counter < working_hours.length)
            {
                var time = parseInt(working_hours[counter].datetime);
                if(time >= start && time <= end)
                {
                    hr_itm.value.push(parseFloat(working_hours[counter].value));
                    conversation_itm.push(parseFloat(conversation[counter].value));

                    if(people[counter].value != "")
                    {
                        if(!DashboardService.isPersonExist(returnObject.people, people[counter].value))
                        {
                            returnObject.people.push(DashboardService.getPersonItem(people[counter].value));
                        }
                    }
                }
                counter++;
            }

            returnObject.hours.push(hr_itm);
            returnObject.conversation.push(conversation_itm);

        });

        return returnObject;
    },
    getPersonItem: function(id)
    {
        for(var p = 0; p < cache_people.length; p++)
        {
            var person = cache_people[p];
            if(person.id == id)
            {
                return person;
            }
        }        
        return undefined;
    },
    isPersonExist: function(collection, id)
    {
        for(var p = 0; p < collection.length; p++)
        {
            var person = collection[p];
            if(person.id == id)
            {
                return true;
            }
        }
        
        return false;
    }
};